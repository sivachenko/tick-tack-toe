﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using Tictactoe.Contracts;

namespace Tictactoe.DesktopViews
{
    /// <summary>
    /// Defines control that can communicate with Presenter
    /// </summary>
    public partial class GameViewControl : UserControl, IGameView
    {
        Square[,] _squareCache; 

        class Indexes
        {
            public int I { get; set; }
            public int J { get; set; }
        }

        public GameViewControl()
        {
            InitializeComponent();
        }

        #region IView<IGameView,IGamePresenter> Members

        private IGamePresenter _presenter;

        /// <summary>
        /// Attach to presenter
        /// </summary>
        /// <param name="presenter">Presenter</param>
        /// <param name="requiresInitialState">Requires initial state update</param>
        public void AttachToPresenter(IGamePresenter presenter, bool requiresInitialState)
        {
            if (presenter == null)
                throw new ArgumentNullException("presenter");

            // detach from previous presenter, if any
            DetachFromPresenter();

            _presenter = presenter;

            // notify presenter that view is connecting
            Presenter.ConnectView(this, requiresInitialState);
        }

        /// <summary>
        /// Detach from presenter
        /// </summary>
        public void DetachFromPresenter()
        {
            lock(this)
            {
                if (Presenter != null)
                {
                    Presenter.DisconnectView(this);
                    _presenter = null;
                }
            }
        }

        /// <summary>
        /// Gets current presenter inctance
        /// </summary>
        public IGamePresenter Presenter
        {
            get 
            {
                return _presenter;
            }
        } 
        #endregion

        #region IGameView Members
        public void EnableStart()
        {
            Dispatcher.BeginInvoke(new Action(() =>
                {
                    btnStart.Visibility = System.Windows.Visibility.Visible;
                }));
        }

        public void DisableStart()
        {
            Dispatcher.BeginInvoke(new Action(() =>
                {
                    btnStart.Visibility = System.Windows.Visibility.Hidden;
                }));
        }

        public void EnableStop()
        {
            Dispatcher.BeginInvoke(new Action(() =>
                {
                    btnStop.Visibility = System.Windows.Visibility.Visible;
                }));
        }

        public void DisableStop()
        {
            Dispatcher.BeginInvoke(new Action(() =>
                {
                    btnStop.Visibility = System.Windows.Visibility.Hidden;
                }));
        }

        public void SetPlayerName(string name)
        {
            Dispatcher.BeginInvoke(new Action(() =>
                {
                    lblPlayerName.Content = name;
                }));
        }

        public void SetTimeLimit(string text)
        {
            Dispatcher.BeginInvoke(new Action(() =>
                {
                    lblTimeLimit.Content = text;
                }));
        }

        public void SetPlayerStatus(string status)
        {
            Dispatcher.Invoke(() =>
                {
                    MessageBox.Show(status);
                });
        }

        public void ShowTurnResult(string result)
        {
            Dispatcher.Invoke(() =>
                {
                    MessageBox.Show(result);
                });
        }

        /// <summary>
        /// Prepare board to accept squares
        /// </summary>
        /// <param name="n">Height in squares</param>
        /// <param name="m">Width in squares</param>
        public void ResetBoard(int m, int n)
        {
            Dispatcher.Invoke(() =>
                {
                    gridSquares.RowDefinitions.Clear();
                    gridSquares.ColumnDefinitions.Clear();

                    for (int i = 0; i < m; i++)
                        gridSquares.RowDefinitions.Add(new RowDefinition());

                    for (int j = 0; j < n; j++)
                        gridSquares.ColumnDefinitions.Add(new ColumnDefinition());
                });

            _squareCache = new Square[m, n];
        }

        /// <summary>
        /// Add square to the board
        /// </summary>
        /// <param name="i">Row number from the top (zero numbered)</param>
        /// <param name="j">Column number from the left (zero numbered)</param>
        public void AddSquare(int i, int j)
        {
            // check if we have square at the same position
            if (_squareCache[i, j] != null)
                throw new ArgumentException(string.Format("Square already added to position [{0},{1}].", i, j));

            Dispatcher.Invoke(() =>
                {
                    var square = new Square();

                    // store indexes into Tag to get in common Click handler
                    square.Tag = new Indexes { I = i, J = j };

                    square.Click += Square_Click;

                    // position on the grid
                    Grid.SetRow(square, i);
                    Grid.SetColumn(square, j);

                    // add to the grid
                    gridSquares.Children.Add(square);

                    // store in the cache
                    _squareCache[i, j] = square;

                });
        }

        void Square_Click(object sender, RoutedEventArgs e)
        {
            var indexes = (sender as Square).Tag as Indexes;

            _presenter.SquareClicked(indexes.I, indexes.J);
        }

        public void PlacePiece(int i, int j, int pieceType)
        {
            var square = _squareCache[i, j];

            if (square == null)
                throw new ArgumentException(string.Format("There isn't square at position [{0},{1}].", i, j));

            if (square.PlacedPiece != null)
                throw new ArgumentException(string.Format("Piece already placed on the square at position [{0},{1}].", i, j));

            square.PlacedPiece = pieceType;
        } 
        #endregion

        private void StartButton_Click(object sender, RoutedEventArgs e)
        {
            _presenter.StartGame();
        }

        private void StopButton_Click(object sender, RoutedEventArgs e)
        {
            // it is just boom, I don't want to carefully stop game thread
            Dispatcher.InvokeShutdown();
        }
    }
}
