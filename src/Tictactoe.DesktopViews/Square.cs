﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Markup;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Xml;

namespace Tictactoe.DesktopViews
{
    /// <summary>
    /// Defines a WPF control that can show one of the piece types. 
    /// </summary>
    /// <remarks>
    /// Piece types are numbered from zero (null value removes piece from square).
    /// Additionaly, it is possible to modify piece style by adding some XAML to />
    /// </remarks>
    public class Square : ContentControl
    {
        #region Exception messages
        /// <summary>
        /// Contain list of messages to use in code
        /// </summary>
        public class Messages
        {
            /// <summary>
            /// Used in exceptions when negative value cannot be set
            /// </summary>
            public const string PlacedPieceLessThenZeroException = "PlacedPiece cann't be less then zero.";
        }
        #endregion

        #region PlacedPiece property
        /// <summary>
        /// Gets or sets the type of piece. Null removes piece.
        /// </summary>
        public int? PlacedPiece
        {
            get { return (int?)GetValue(PlacedPieceProperty); }
            set { SetValue(PlacedPieceProperty, value); }
        }
        
        /// <summary>
        /// Identifies the PlacedPiece dependency property.
        /// </summary>
        public static readonly DependencyProperty PlacedPieceProperty;
        #endregion

        #region PiecesTemplates property
        /// <summary>
        /// Gets or sets the list of custom styles for pieces.
        /// </summary>
        public UIElement[] PiecesTemplates
        {
            get { return (UIElement[])GetValue(PiecesTemplatesProperty); }
            set { SetValue(PiecesTemplatesProperty, value); }
        }

        /// <summary>
        /// Identifies the PiecesTemplates dependency property.
        /// </summary>
        public static readonly DependencyProperty PiecesTemplatesProperty;
        #endregion

        #region Click routed event
        public static RoutedEvent ClickEvent;

        public event RoutedEventHandler Click
        {
            add { AddHandler(ClickEvent, value); }
            remove { RemoveHandler(ClickEvent, value); }
        }

        protected virtual void OnClick()
        {
            RoutedEventArgs args = new RoutedEventArgs(ClickEvent, this);

            RaiseEvent(args);

        }

        protected override void OnMouseLeftButtonUp(MouseButtonEventArgs e)
        {
            base.OnMouseLeftButtonUp(e);

            OnClick();
        } 
        #endregion

        static Square()
        {
            DefaultStyleKeyProperty.OverrideMetadata(typeof(Square), new FrameworkPropertyMetadata(typeof(Square)));

            PlacedPieceProperty = DependencyProperty.Register("PlacedPiece", typeof(int?), typeof(Square),
                new FrameworkPropertyMetadata(null, FrameworkPropertyMetadataOptions.AffectsArrange, new PropertyChangedCallback(PlacedPieceChanged)));

            PiecesTemplatesProperty = DependencyProperty.Register("PiecesTemplates", typeof(UIElement[]), typeof(Square), 
                new FrameworkPropertyMetadata(null, FrameworkPropertyMetadataOptions.AffectsArrange));

            ClickEvent = EventManager.RegisterRoutedEvent("Click", RoutingStrategy.Bubble, typeof(RoutedEventHandler), typeof(Square));
        }


        /// <summary>
        /// Updates content property of control when PlacedPiece property changed
        /// </summary>
        private static void PlacedPieceChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            var sq = (Square)d;

            // if value is null remove piece from square
            if (e.NewValue == null)
            {
                sq.Content = null;
                return;
            }

            int pieceNumber = (int)e.NewValue;

            if (pieceNumber < 0)
                throw new ArgumentOutOfRangeException("PlacedPiece", pieceNumber, Messages.PlacedPieceLessThenZeroException);

            // if exists piece template apply it, else use simple lable
            if (sq.PiecesTemplates != null && pieceNumber < sq.PiecesTemplates.Length)
            {
                // because we cann't just place the same UIElement twice, 
                // copy it through serialization
                string xaml = XamlWriter.Save(sq.PiecesTemplates[pieceNumber]);

                StringReader stringReader = new StringReader(xaml);

                XmlReader xmlReader = XmlReader.Create(stringReader);
                
                sq.Content = XamlReader.Load(xmlReader);
            }
            else
            {
                var lbl = new Label();
                lbl.Content = pieceNumber;

                sq.Content = lbl;
            }
        }
    }
}
