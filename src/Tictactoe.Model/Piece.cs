﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Tictactoe.Contracts.Model;

namespace Tictactoe.Model
{
    /// <summary>
    /// Represents domain model piece.
    /// </summary>
    public class Piece : IPiece
    {
        private IPlayer player;

        /// <summary>
        /// Initializes a new instance of the <see cref="Piece"/>, link it with player.
        /// </summary>
        /// <param name="player">Player of the game.</param>
        public Piece(IPlayer player)
        {
            this.player = player;
        }

        /// <summary>
        /// Gets player who owns the piece.
        /// </summary>
        /// <returns>Player who owns the piece.</returns>
        public IPlayer GetPieceOwner()
        {
            return player;
        }
    }
}
