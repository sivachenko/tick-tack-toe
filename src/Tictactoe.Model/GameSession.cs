﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using Tictactoe.Contracts.Model;

namespace Tictactoe.Model
{
    /// <summary>
    /// Manages current game turn, maintains time limit records.
    /// TODO: it should be renamed to more appropriate name.
    /// </summary>
    public class GameSession : IGameSession
    {
        private List<IPlayer> playersList;
        private IGameRule gameRule;

        IBoard _board;

        Dictionary<IPlayer, double> _timeLimits = new Dictionary<IPlayer, double>();


        // it should be atomic and thread safe
        volatile bool isPlacingAllowed = false;

        /// <summary>
        ///  Occurs when piece is successfully placed on the board.
        /// </summary>
        public event EventHandler<PiecePlacedEventArgs> PiecePlaced;

        /// <summary>
        /// Initializes a new instance of the <see cref="GameSession"/>, using players list and rules.
        /// </summary>
        /// <param name="playersList">List of players.</param>
        /// <param name="gameRule">Current game rules.</param>
        public GameSession(List<IPlayer> playersList, IGameRule gameRule)
        {
            this.playersList = playersList;
            this.gameRule = gameRule;
        }

        /// <summary>
        /// Gets current game board.
        /// </summary>
        public IBoard Board
        {
            get
            {
                return _board;
            }
        }

        /// <summary>
        /// Gets list of players.
        /// </summary>
        public List<IPlayer> PlayersList
        {
            get 
            { 
                return playersList; 
            }
        }

        /// <summary>
        /// Allows to place new pieces on the board. Method <see cref="GameSession.PlacePiece"/> can return true.
        /// </summary>
        public void AllowPiecePlacing()
        {
            isPlacingAllowed = true;
        }

        /// <summary>
        /// Deny to place new pieces on the board. Method <see cref="GameSession.PlacePiece"/> should return false.
        /// </summary>
        public void DenyPiecePlacing()
        {
            isPlacingAllowed = false;
        }

        /// <summary>
        /// Verify square state and place piece on it.
        /// TODO: should implement inversion of control to smoothly synchronize calling and game threads.
        /// </summary>
        /// <param name="square">Square to place a piece.</param>
        /// <param name="player">Player whose piece will be placed.</param>
        /// <returns>Validation result. True if successfully placed.</returns>
        public bool PlacePiece(ISquare square, IPlayer player)
        {
            // discard if placing is disabled
            if (!isPlacingAllowed)
                return false;

            // discard if piece is already placed
            if (!square.IsEmpty)
                return false;

            square.PlacePiece(player.MakePiece());

            OnPiecePlaced(null);

            return true;
        }

        /// <summary>
        /// Raises the <see cref="PiecePlaced"/> event.
        /// </summary>
        /// <param name="eventArgs">An <see cref="PiecePlacedEventArgs"/> that contain event data.</param>
        protected virtual void OnPiecePlaced(PiecePlacedEventArgs eventArgs)
        {
            if (PiecePlaced != null)
                PiecePlaced(this, eventArgs);
        }

        /// <summary>
        /// Makes a move for player, updates player's time limit and active state.
        /// </summary>
        /// <param name="player">Player to make a move.</param>
        public void MakeMove(IPlayer player)
        {
            var limit = _timeLimits[player];

            // call method and measure elapsed time
            var beginTime = DateTime.Now;

            player.MakeMove(this, limit);

            var endTime = DateTime.Now;

            // calculate new time limit
            var newLimit = limit - (endTime - beginTime).TotalSeconds;

            _timeLimits[player] = newLimit;

            // set player inactive if time is up
            if (newLimit <= 0)
                player.Active = false;
        }

        /// <summary>
        /// Restarts session to allow new game to play.
        /// </summary>
        public void Restart()
        {
            // make new board 
            _board = gameRule.MakeBoard();

            // activate players and recalculate all time limits in dictionary
            _timeLimits.Clear();

            foreach(var player in playersList)
            {
                player.Active = true;

                var limit = gameRule.GetTimeLimit(player);

                _timeLimits.Add(player, limit);
            }
        }

        /// <summary>
        /// Get time limit left for player.
        /// </summary>
        /// <param name="player">Player.</param>
        /// <returns>Time in fractional seconds.</returns>
        public double GetTimeLimit(IPlayer player)
        {
            return _timeLimits[player];
        }

        /// <summary>
        /// Gets player, who will make a first move according to game rules.
        /// </summary>
        /// <returns>Player whose move starts the game.</returns>
        public IPlayer GetFirstPlayer()
        {
            return gameRule.GetFirstPlayer(playersList);
        }

        /// <summary>
        /// Gets player, who will make the next move according to game rules.
        /// </summary>
        /// <param name="currentPlayer">Current player.</param>
        /// <returns>Player who will make a move next.</returns>
        public IPlayer GetNextPlayer(IPlayer currentPlayer)
        {
            return gameRule.GetNextPlayer(playersList, currentPlayer);
        }

        /// <summary>
        /// Gets current turn result.
        /// </summary>
        /// <returns>Turn results.</returns>
        public ITurnResult GetTurnResult()
        {
            return gameRule.GetTurnResult(playersList, _board);
        }
    }
}
