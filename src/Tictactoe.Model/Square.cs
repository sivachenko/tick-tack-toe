﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Tictactoe.Contracts.Model;

namespace Tictactoe.Model
{
    /// <summary>
    /// Represents cell on the game board.
    /// </summary>
    public class Square : ISquare
    {
        IPiece _piece = null;

        /// <summary>
        /// Gets state about is the square empty.
        /// </summary>
        public bool IsEmpty
        {
            get
            {
                return _piece == null;
            }
        }

        /// <summary>
        /// Place the piece on the square.
        /// </summary>
        /// <param name="piece">Piece to be placed.</param>
        public void PlacePiece(IPiece piece)
        {
            if (piece == null)
                throw new ArgumentNullException("piece", "Cannot store empty piece.");

            _piece = piece;
        }

        /// <summary>
        /// Gets piece already placed on the square. Returns null if square is empty.
        /// </summary>
        /// <returns>Piece or null if square is empty.</returns>
        public IPiece GetPiece()
        {
            return _piece;
        }
    }
}
