﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Tictactoe.Contracts.Model;

namespace Tictactoe.Model
{
    /// <summary>
    /// Encapsulates results of the game turn.
    /// </summary>
    public class TurnResult : ITurnResult
    {
        /// <summary>
        /// Gets list of winners. Empty if there is no winners.
        /// </summary>
        public List<IPlayer> Winners { get; set; }

        /// <summary>
        /// Gets value either next turn can be made.
        /// </summary>
        public bool CanMakeNextTurn { get; set; }

        /// <summary>
        /// Gets value is game ended.
        /// </summary>
        public bool IsGameEnded { get; set; }

        /// <summary>
        /// Gets text message that can be shown on the results of the turn.
        /// </summary>
        public string Message { get; set; }
    }
}
