﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Tictactoe.Contracts.Model;

namespace Tictactoe.Model
{
    /// <summary>
    /// Encapsulates squares. Provides access to them via two-dimensional index.
    /// </summary>
    public class Board : IBoard
    {
        private int[,] boardTemplate;

        private ISquare[,] _squares;

        /// <summary>
        /// Initializes a new instance of the <see cref="Board"/>, using the specified board template.
        /// </summary>
        /// <param name="boardTemplate">Two-dimensional array, non-zero values corresponds to squares, zero - to holes.</param>
        public Board(int[,] boardTemplate)
        {
            this.boardTemplate = boardTemplate;

            int M = boardTemplate.GetLength(0);
            int N = boardTemplate.GetLength(1);

            _squares = new ISquare[M, N];

            for(int i=0; i < M; i++)
                for(int j=0; j < N; j++)
                {
                    if (boardTemplate[i, j] != 0)
                        _squares[i, j] = new Square();
                }
        }

        #region Interface IBoard
        /// <summary>
        ///  Count of rows of the board
        /// </summary>
        public int SizeM
        {
            get { return _squares.GetLength(0); }
        }

        /// <summary>
        /// Count of columns of the board
        /// </summary>
        public int SizeN
        {
            get { return _squares.GetLength(1); }
        }

        /// <summary>
        /// Gets the square at position [i, j]
        /// </summary>
        /// <param name="i">Row index</param>
        /// <param name="j">Column index</param>
        /// <returns>The square at position [i, j].</returns>
        public ISquare this[int i, int j]
        {
            get
            {
                return _squares[i, j];
            }
        } 
        #endregion
    }
}
