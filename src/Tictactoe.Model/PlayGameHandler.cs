﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using Tictactoe.Contracts.Model;

// Lets share some methods for testing
[assembly: InternalsVisibleTo("Tictactoe.Model.Tests")]

namespace Tictactoe.Model
{
    /// <summary>
    /// Represents use case...
    /// TODO: Which one? We don't have Play Game...
    /// </summary>
    public class PlayGameHandler : IPlayGameHandler
    {
        private object _syncLock = new object();

        private IPlayer _currentPlayer;
        
        private IGameSession gameSession;

        private volatile bool shouldStop;

        /// <summary>
        /// Initializes a new instance of the <see cref="PlayGameHandler"/>, using game session.
        /// </summary>
        /// <param name="gameSession">Game session to perform turns.</param>
        public PlayGameHandler(IGameSession gameSession)
        {
            this.gameSession = gameSession;
        }

        /// <summary>
        /// The main game loop. This method can be tested.
        /// </summary>
        internal void RunGame()
        {
            // reset game session it should prepare new board and so on
            gameSession.Restart();

            // raise an event about starting
            OnGameStarted(new GameStartedArgs(gameSession.PlayersList, gameSession.Board));

            // get first player according to game rules
            lock (_syncLock)
            {
                _currentPlayer = gameSession.GetFirstPlayer();
            }

            // setup flag to stop a thread
            shouldStop = false;

            // main game loop
            while (_currentPlayer != null && !shouldStop)
            {
                // get time limit for current player
                var turnTimeLimit = gameSession.GetTimeLimit(_currentPlayer);

                // notify others that new player begins to play
                OnPlayerChanged(new PlayerChangedArgs(_currentPlayer, turnTimeLimit));

                // make a move
                gameSession.MakeMove(_currentPlayer);

                // player can become inactivated 
                var playerStatus = _currentPlayer.Active;

                // calculate result of this turn
                var turnResult = gameSession.GetTurnResult();

                // notify others thar turn completed with results
                OnTurnCompleted(new TurnCompletedArgs(playerStatus, turnResult));

                // check if we need to stop loop
                if (turnResult.IsGameEnded)
                    shouldStop = true;

                // get next player if need to play next turn
                if (!shouldStop)
                    lock (_syncLock)
                    {
                        _currentPlayer = gameSession.GetNextPlayer(_currentPlayer);
                    }
            }

            // notify other that game is ended
            OnGameEnded(new GameEndedArgs());
        }

        #region PlayerChanged event
        /// <summary>
        /// Occurs when next (or first) player is selected.
        /// </summary>
        public event EventHandler<PlayerChangedArgs> PlayerChanged;

        /// <summary>
        /// Raises the <see cref="PlayerChanged"/> event.
        /// </summary>
        /// <param name="args">An <see cref="PlayerChangedArgs"/> that contains event data.</param>
        void OnPlayerChanged(PlayerChangedArgs args)
        {
            if (PlayerChanged != null)
                PlayerChanged(this, args);
        }
        
        #endregion

        #region GameStarted event
        /// <summary>
        /// Occurs when new game session is initialized (or reinitialized).
        /// </summary>
        public event EventHandler<GameStartedArgs> GameStarted;

        /// <summary>
        /// Raises the <see cref="GameStarted"/> event.
        /// </summary>
        /// <param name="args">An <see cref="GameStartedArgs"/> that contains the event data.</param>
        void OnGameStarted(GameStartedArgs args)
        {
            if (GameStarted != null)
                GameStarted(this, args);
        } 
        #endregion

        #region TurnCompleted event
        /// <summary>
        /// Occurs when the next turn is completed.
        /// </summary>
        public event EventHandler<TurnCompletedArgs> TurnCompleted;

        /// <summary>
        /// Raises the <see cref="TurnCompleted"/> event.
        /// </summary>
        /// <param name="args">An <see cref="TurnCompletedArgs"/> that contains the event data.</param>
        void OnTurnCompleted(TurnCompletedArgs args)
        {
            if (TurnCompleted != null)
                TurnCompleted(this, args);
        } 
        #endregion

        #region GameEnded event
        /// <summary>
        /// Occurs when game ended.
        /// </summary>
        public event EventHandler<GameEndedArgs> GameEnded;

        /// <summary>
        /// Raises the <see cref="GameEnded"/> event.
        /// </summary>
        /// <param name="args">An <see cref="GameEndedArgs"/> that contains the event data.</param>
        void OnGameEnded(GameEndedArgs args)
        {
            if (GameEnded != null)
                GameEnded(this, args);
        } 
        #endregion

        /// <summary>
        /// Handles system operation PlayGame. Starts new game thread.
        /// </summary>
        public void PlayGame()
        {
            Thread gameThread = new Thread(() =>
                {
                    RunGame();
                });

            gameThread.IsBackground = true;
            gameThread.Start();
        }

        /// <summary>
        /// Handles system operation PlacePiece.
        /// </summary>
        /// <param name="square">Square to place a piece.</param>
        /// <returns>Validation result. True if placed successfully.</returns>
        public bool PlacePiece(ISquare square)
        {
            lock (_syncLock)
            {
                return gameSession.PlacePiece(square, _currentPlayer);
            }
        }
    }
}
