﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Tictactoe.Contracts.Model;

namespace Tictactoe.Model
{
    /// <summary>
    /// Defines rules of the game. 
    /// </summary>
    public class GameRule : IGameRule
    {
        const double GameTimeLimit = 30;

        /// <summary>
        /// Hard-coded template. TODO: should be protected property.
        /// </summary>
        int[,] BoardTemplate = new int[,] {                                     
            {1, 1, 1},
            {1, 1, 1},
            {1, 1, 1}};

        /// <summary>
        /// Creates a board according to game rules.
        /// </summary>
        /// <returns>Current game board.</returns>
        public IBoard MakeBoard()
        {
            return new Board(BoardTemplate);
        }

        /// <summary>
        /// Checks position of pieces on the board and calculates game state on the end of the current turn.
        /// </summary>
        /// <param name="playersList">List of players.</param>
        /// <param name="board">Game board.</param>
        /// <returns>Turn results.</returns>
        public ITurnResult GetTurnResult(List<IPlayer> playersList,  IBoard board)
        {
            var result = new TurnResult();

            // this is very stupid algorithm, 
            // for better one we should track each addition of pieces
            result.Winners = new List<IPlayer>();

            result.CanMakeNextTurn = false;
            result.IsGameEnded = false;

            #region Check rows
            Dictionary<IPlayer, int> rowinfo = new Dictionary<IPlayer, int>();

            // for every row
            for (int i = 0; i < board.SizeM; i++)
            {
                rowinfo.Clear();

                for (int j = 0; j < board.SizeN; j++)
                {
                    var sq = board[i, j];

                    // if row contains hole, skip the row
                    if (sq == null)
                        break;

                    // if row contain empty square, skip the row and save this info
                    if (sq.IsEmpty)
                    {
                        result.CanMakeNextTurn = true;
                        break;
                    }

                    var key = sq.GetPiece().GetPieceOwner();

                    int val;

                    // if found, increment it
                    if (rowinfo.TryGetValue(key, out val))
                        rowinfo[key] = ++val;
                    else
                        rowinfo[key] = 1;
                }

                // check if only one player fill up the row
                if (rowinfo.Keys.Count == 1 && rowinfo.Values.ToArray()[0] == board.SizeN)
                    result.Winners.Add(rowinfo.Keys.ToArray()[0]);
            }
            #endregion

            #region Check columns
            Dictionary<IPlayer, int> colinfo = new Dictionary<IPlayer, int>();

            // for every row
            for (int j = 0; j < board.SizeN; j++)
            {
                colinfo.Clear();

                for (int i = 0; i < board.SizeM; i++)
                {
                    var sq = board[i, j];

                    // if row contains hole, skip the row
                    if (sq == null)
                        break;

                    // if row contain empty square, skip the row and save this info
                    if (sq.IsEmpty)
                    {
                        result.CanMakeNextTurn = true;
                        break;
                    }

                    var key = sq.GetPiece().GetPieceOwner();

                    int val;

                    // if found, increment it
                    if (colinfo.TryGetValue(key, out val))
                        colinfo[key] = ++val;
                    else
                        colinfo[key] = 1;
                }

                // check if only one player fill up the row
                if (colinfo.Keys.Count == 1 && colinfo.Values.ToArray()[0] == board.SizeM)
                    result.Winners.Add(colinfo.Keys.ToArray()[0]);
            }
            #endregion

            #region Check main diagonals
            
            // add two more loops to work with rectangles
            int lhdiagCount = board.SizeN - board.SizeM + 1;
            int lvdiagCount = board.SizeM - board.SizeN + 1;

            // which delta less or equal zero, make it one to enter into the loop
            if (lhdiagCount <= 0)
                lhdiagCount = 1;

            if (lvdiagCount <= 0)
                lvdiagCount = 1;

            // get minimum from sizes
            int lminSize = Math.Min(board.SizeM, board.SizeN);


            Dictionary<IPlayer, int> linfo = new Dictionary<IPlayer, int>();

            // for two variants of recrangles and one diagonal
            for (int di = 0; di < lvdiagCount; di++)
            {
                for (int dj = 0; dj < lhdiagCount; dj++)
                {
                    linfo.Clear();

                    for (int k = 0; k < lminSize; k++)
                    {
                        var sq = board[di + k, dj + k];

                        // if row contains hole, skip the row
                        if (sq == null)
                            break;

                        // if row contain empty square, skip the row and save this info
                        if (sq.IsEmpty)
                        {
                            result.CanMakeNextTurn = true;
                            break;
                        }

                        var key = sq.GetPiece().GetPieceOwner();

                        int val;

                        // if found, increment it
                        if (linfo.TryGetValue(key, out val))
                            linfo[key] = ++val;
                        else
                            linfo[key] = 1;
                    }

                    // check if only one player fill up the row
                    if (linfo.Keys.Count == 1 && linfo.Values.ToArray()[0] == lminSize)
                        result.Winners.Add(linfo.Keys.ToArray()[0]);
                }
            }
            
            #endregion

            #region Check antidiagonals

            // add two more loops to work with rectangles
            int rhdiagCount = board.SizeN - board.SizeM + 1;
            int rvdiagCount = board.SizeM - board.SizeN + 1;

            // which delta less or equal zero, make it one to enter into the loop
            if (rhdiagCount <= 0)
                rhdiagCount = 1;

            if (rvdiagCount <= 0)
                rvdiagCount = 1;

            // get minimum from sizes
            int rminSize = Math.Min(board.SizeM, board.SizeN);


            Dictionary<IPlayer, int> rinfo = new Dictionary<IPlayer, int>();

            // for two variants of recrangles and one diagonal
            for (int di = 0; di < rvdiagCount; di++)
            {
                for (int dj = 0; dj < rhdiagCount; dj++)
                {
                    rinfo.Clear();

                    for (int k = 0; k < rminSize; k++)
                    {
                        var sq = board[di + k, dj + rminSize - k - 1];

                        // if row contains hole, skip the row
                        if (sq == null)
                            break;

                        // if row contain empty square, skip the row and save this info
                        if (sq.IsEmpty)
                        {
                            result.CanMakeNextTurn = true;
                            break;
                        }

                        var key = sq.GetPiece().GetPieceOwner();

                        int val;

                        // if found, increment it
                        if (rinfo.TryGetValue(key, out val))
                            rinfo[key] = ++val;
                        else
                            rinfo[key] = 1;
                    }

                    // check if only one player fill up the row
                    if (rinfo.Keys.Count == 1 && rinfo.Values.ToArray()[0] == rminSize)
                        result.Winners.Add(rinfo.Keys.ToArray()[0]);
                }
            }
            #endregion

            // check either exists any active player
            // if not end the game
            if (playersList.All((m) => !m.Active))
            {
                result.CanMakeNextTurn = false;
                result.IsGameEnded = true;
            }

            // calculate stop conditions
            if (result.Winners.Count == 0)
            {
                if (!result.CanMakeNextTurn)
                    result.IsGameEnded = true;
            }
            else
                // lets play game to the first winner
                result.IsGameEnded = true;

            // process message
            if (result.Winners.Count == 0)
            {
                if (result.CanMakeNextTurn)
                    result.Message = "Ready to next turn.";
                else
                    result.Message = "Draw game.";
            }
            else
            {
                result.Message = string.Format("Winners: {0}.",
                    string.Join(", ", result.Winners.Select((w) => w.Name).ToArray()));
            }

            return result;
        }

        /// <summary>
        /// Calculates player, who will make a first move according to game rules.
        /// </summary>
        /// <param name="playersList">List of players</param>
        /// <returns>Player whose move starts the game.</returns>
        public IPlayer GetFirstPlayer(List<IPlayer> playersList)
        {
            foreach(var p in playersList)
            {
                if (p.Active)
                    return p;
            }

            return null;
        }

        /// <summary>
        /// Calculates player, who will make the next move according to game rules.
        /// </summary>
        /// <param name="playersList">List of players.</param>
        /// <param name="currentPlayer">Current player.</param>
        /// <returns>Player who will make a move next.</returns>
        public IPlayer GetNextPlayer(List<IPlayer> playersList, IPlayer currentPlayer)
        {
            int index = playersList.IndexOf(currentPlayer);

            for (int i = 0; i < playersList.Count; i++)
            {
                int cyclicNext = (index + i + 1) % playersList.Count;

                if (playersList[cyclicNext].Active)
                    return playersList[cyclicNext];
            }

            return null;
        }

        /// <summary>
        /// Calculates an initial total time limit for all player's turns according to game rules.
        /// </summary>
        /// <param name="player">Player.</param>
        /// <returns>Time in fractional seconds.</returns>
        public double GetTimeLimit(IPlayer player)
        {
            return GameTimeLimit - player.TimePenalty;
        }
    }
}
