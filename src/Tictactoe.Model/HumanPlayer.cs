﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using Tictactoe.Contracts.Model;

namespace Tictactoe.Model
{
    /// <summary>
    /// Defines human player. Class waits until piece will be placed by human.
    /// </summary>
    public class HumanPlayer : IPlayer
    {
        AutoResetEvent _eventPiecePlaced = new AutoResetEvent(false);

        /// <summary>
        /// Gets or sets player name.
        /// </summary>
        public string Name
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets player time penalty.
        /// </summary>
        public double TimePenalty
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets player state. If player inactive, they cannot make a move.
        /// </summary>
        public bool Active
        {
            get;
            set;
        }

        /// <summary>
        /// Performs player move.
        /// </summary>
        /// <param name="gameSession">Current game session in which move will be made.</param>
        /// <param name="timeLimit">Time limit of the current turn.</param>
        public void MakeMove(IGameSession gameSession, double timeLimit)
        {
            if (!Active)
                throw new InvalidOperationException("Cannot make a move when player is inactive.");

            // subscribe to successful piece placing
            gameSession.PiecePlaced += gameSession_PiecePlaced;

            // allow placing
            gameSession.AllowPiecePlacing();
            
            // wait until placed, but no more than time limit
            _eventPiecePlaced.WaitOne((int)Math.Round(timeLimit * 1000));

            // deny placing
            gameSession.DenyPiecePlacing();

            // unsubscribe from event
            gameSession.PiecePlaced -= gameSession_PiecePlaced;
        }

        void gameSession_PiecePlaced(object sender, PiecePlacedEventArgs e)
        {
            // release MakeMove method
            _eventPiecePlaced.Set();
        }

        /// <summary>
        /// Creates a piece for current player.
        /// </summary>
        /// <returns>Piece for current player.</returns>
        public IPiece MakePiece()
        {
            return new Piece(this);
        }
    }
}
