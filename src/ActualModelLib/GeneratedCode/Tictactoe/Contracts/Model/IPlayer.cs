﻿//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a tool
//     Changes to this file will be lost if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------
namespace Tictactoe.Contracts.Model
{
	using System;
	using System.Collections.Generic;
	using System.Linq;
	using System.Text;

	/// <summary>
	/// Defines player of the game.
	/// </summary>
	public interface IPlayer 
	{
		/// <summary>
		/// Gets or sets a player name
		/// </summary>
		string Name { get;set; }

		/// <summary>
		/// Gets or sets player time penalty.
		/// </summary>
		double TimePenalty { get;set; }

		/// <summary>
		/// Gets or sets player state. If player inactive, they cannot make a move.
		/// </summary>
		bool Active { get;set; }

		/// <summary>
		/// Performs player move.
		/// </summary>
		/// <param name="gameSession">Current game session in which move will be made.</param>
		/// <param name="timeLimit">Time limit of the current turn.</param>
		void MakeMove(IGameSession gameSession, double timeLimit);

		/// <summary>
		/// Creates a piece for current player.
		/// </summary>
		IPiece MakePiece();

	}
}

