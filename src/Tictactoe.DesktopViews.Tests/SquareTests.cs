﻿using System;
using System.Windows;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Windows.Controls;
using System.Windows.Markup;

namespace Tictactoe.DesktopViews.Tests
{
    [TestClass]
    public class SquareTests
    {
        /// <summary>
        /// Check that just after initialization square is empty
        /// </summary>
        [TestMethod]
        [TestCategory("UI")]
        [TestCategory("CI")]
        public void Square_WhenCreated_InitializePlacedPieceAsNull()
        {
            // arrange
            var sq = new Square();

            // act

            // asset
            Assert.IsNull(sq.PlacedPiece, "Just after initialization PlacesPiece not null.");
        }

        /// <summary>
        /// Tests that PiecePlaced property implemented via Dependency Property
        /// </summary>
        [TestMethod]
        [TestCategory("UI")]
        [TestCategory("CI")]
        public void Square_WhenSetPiecePlaced_UpdatesDependencyProperty()
        {
            // arrange
            int expected = 2;
            var sq = new Square();

            // act
            sq.PlacedPiece = expected;

            // assert
            var actual = (int?)sq.GetValue(Square.PlacedPieceProperty);

            Assert.IsNotNull(actual, "PlacedPiece dependency property returns null.");
            Assert.AreEqual(expected, actual, "PlacedPiece dependency property not set correctly.");
        }

        /// <summary>
        /// Tests that PiecesTemplates property implemented via Dependency Property
        /// </summary>
        [TestMethod]
        [TestCategory("UI")]
        [TestCategory("CI")]
        public void Square_WhenSetPiecesTemplates_UpdatesDependencyProperty()
        {
            // arrange
            var expected = new Button[] { new Button(), new Button() };

            var sq = new Square();

            // act
            sq.PiecesTemplates = expected;

            // assert
            var actual = (Button[])sq.GetValue(Square.PiecesTemplatesProperty);

            Assert.IsNotNull(actual, "PiecesTemplates dependency property returns null.");
            Assert.AreSame(expected, actual, "PiecesTemplates dependency property not set correctly.");
        }

        /// <summary>
        /// Tests that changes in PiecePlaced bring to changes in control content
        /// </summary>
        [TestMethod]
        [TestCategory("UI")]
        [TestCategory("CI")]
        public void Square_WhenSetPiecePlaced_SelectsRightPieceType()
        {
            // arrange
            var expected = new Button();
            expected.Content = "Hello!";

            // create two piece types as buttons
            var pieceTypes = new Button[] { new Button(), expected };

            int piece = 1;

            var sq = new Square();

            // act
            sq.PiecesTemplates = pieceTypes;
            sq.PlacedPiece = piece;

            // assert
            var actual = (Button)sq.Content;

            Assert.AreEqual(XamlWriter.Save(expected), XamlWriter.Save(actual), "PlacedPiece does not set correct content property of control.");
        }

        [TestMethod]
        [TestCategory("UI")]
        [TestCategory("CI")]
        public void Square_WhenSetLessThenZeroPiecePlaced_ShouldThrowArgumentOutOfRange()
        {
            // arrange
            int piece = -5;

            var sq = new Square();

            // act
            try
            {
                sq.PlacedPiece = piece;
            }
            catch(ArgumentOutOfRangeException ex)
            {
                StringAssert.Contains(ex.Message, Square.Messages.PlacedPieceLessThenZeroException,
                    "Exception message is not correct.");
                return;
            }

            // assert
            Assert.Fail("No exception was thrown.");
        }

        
        /// <summary>
        /// And just for fun. :) To achieve 100% coverage of Square class.
        /// </summary>
        [TestMethod]
        [TestCategory("UI")]
        [TestCategory("CI")]
        public void Square_WhenSetNullToPlacedPiece_RemovesPiece()
        {
            // arrange
            var sq = new Square();

            int piece = 1;

            // act
            sq.PlacedPiece = piece;
            sq.PlacedPiece = null;

            // assert
            var actual = sq.Content;

            Assert.IsNull(actual, "PlacedPiece does not remove content from control.");
        }
    }
}
