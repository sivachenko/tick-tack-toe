﻿using Ninject.Modules;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Tictactoe.Contracts;
using Tictactoe.Contracts.Model;
using Tictactoe.Model;
using Tictactoe.Presenters;

namespace Tictactoe.Application
{
    class ApplicationInjectModule : NinjectModule 
    {
        public override void Load()
        {
            Bind<List<IPlayer>>().ToMethod((c) => 
                { 
                    return new List<IPlayer>() { }; 
                });

            Bind<IGameRule>().To<GameRule>();
            Bind<IGameSession>().To<GameSession>();
            Bind<IPlayGameHandler>().To<PlayGameHandler>().InSingletonScope();
            Bind<IGamePresenter>().To<GamePresenter>();
        }

    }
}
