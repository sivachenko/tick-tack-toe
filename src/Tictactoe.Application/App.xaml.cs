﻿using Ninject;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Threading.Tasks;
using System.Windows;
using Tictactoe.Contracts;
using Tictactoe.Contracts.Model;
using Tictactoe.Model;
using Tictactoe.Presenters;

namespace Tictactoe.Application
{
    /// <summary>
    /// Interaction logic for App.xaml
    /// </summary>
    public partial class App : System.Windows.Application
    {
        [STAThread]
        static void Main()
        {
            var app = new App();

            app.Startup += app_Startup;

            app.Run();

        }

        static void app_Startup(object sender, StartupEventArgs e)
        {
            IKernel kernel = new StandardKernel();

            int playerIndex = 1;
            var randPenalty = new Random(DateTime.Now.Second);

            kernel.Bind<IPlayer>().ToMethod((c) =>
                {
                    var player = new HumanPlayer();

                    player.Name = string.Format("Player {0}", playerIndex);
                    playerIndex++;

                    //player.TimePenalty = randPenalty.NextDouble() * 15;

                    return player;
                });


            kernel.Bind<IGameRule>().To<GameRule>();
            kernel.Bind<IGameSession>().ToMethod((c) =>
                {
                    return new GameSession(
                        new List<IPlayer>() { 
                            kernel.Get<IPlayer>(),
                            kernel.Get<IPlayer>()},
                        kernel.Get<IGameRule>());
                });
            kernel.Bind<IPlayGameHandler>().To<PlayGameHandler>().InSingletonScope();
            kernel.Bind<IGamePresenter>().To<GamePresenter>();


            IGamePresenter playGamePresenter = kernel.Get<IGamePresenter>();

            FirstGameWindow w1 = new FirstGameWindow();

            w1.gvcDesktop.AttachToPresenter(playGamePresenter, true);

            w1.Show();

            SecondGameWindow w2 = new SecondGameWindow();

            w2.gvcDesktop.AttachToPresenter(playGamePresenter, true);

            w2.Show();
        }
    }
}
