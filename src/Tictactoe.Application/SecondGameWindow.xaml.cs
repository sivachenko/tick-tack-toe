﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace Tictactoe.Application
{
    /// <summary>
    /// Interaction logic for SecondGameWindow.xaml
    /// </summary>
    public partial class SecondGameWindow : Window
    {
        public SecondGameWindow()
        {
            InitializeComponent();
        }

        private void Window_Closed(object sender, EventArgs e)
        {
            gvcDesktop.DetachFromPresenter();
        }
    }
}
