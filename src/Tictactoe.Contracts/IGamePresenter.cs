﻿using CobaltSoftware.Foundation.ModelViewPresenter;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Tictactoe.Contracts
{
    /// <summary>
    /// Defines a presenter for a game process
    /// </summary>
    public interface IGamePresenter : IPresenter<IGamePresenter, IGameView>
    {
        void StartGame();

        void SquareClicked(int i, int j);
    }
}
