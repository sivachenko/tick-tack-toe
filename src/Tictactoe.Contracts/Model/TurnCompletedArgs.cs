﻿//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a tool
//     Changes to this file will be lost if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------
namespace Tictactoe.Contracts.Model
{
	using System;
	using System.Collections.Generic;
	using System.Linq;
	using System.Text;

	public class TurnCompletedArgs : EventArgs
	{
        public bool IsPlayerActive { get; private set; }

        public ITurnResult TurnResult { get; private set; }

        public TurnCompletedArgs(bool isPlayerActive, ITurnResult turnResult)
        {
            IsPlayerActive = isPlayerActive;
            TurnResult = turnResult;
        }
	}
}

