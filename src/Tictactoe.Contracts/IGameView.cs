﻿using CobaltSoftware.Foundation.ModelViewPresenter;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Tictactoe.Contracts
{
    /// <summary>
    /// Represents some UI that used during game process
    /// </summary>
    public interface IGameView : IView<IGameView, IGamePresenter>
    {
        void EnableStart();
        
        void DisableStart();

        void EnableStop();

        void DisableStop();

        void SetPlayerName(string name);

        void SetTimeLimit(string text);

        void SetPlayerStatus(string status);

        void ShowTurnResult(string result);

        void ResetBoard(int n, int m);

        void AddSquare(int i, int j);

        void PlacePiece(int i, int j, int pieceType);
    }
}
