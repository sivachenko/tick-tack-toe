﻿using CobaltSoftware.Foundation.ModelViewPresenter;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using Tictactoe.Contracts;
using Tictactoe.Contracts.Model;

namespace Tictactoe.Presenters
{
    /// <summary>
    /// Controls and coordinates UI that perfoms game process, e.g. placing pieces
    /// </summary>
    public class GamePresenter : PresenterBase<IGamePresenter, IGameView>, IGamePresenter
    {
        private IPlayGameHandler playGameUC;
        
        private IBoard board;
        
        private List<IPlayer> playersList;

        private IPlayer currentPlayer;


        /// <summary>
        /// Stores int values that will be used by view to select proper template
        /// </summary>
        private Dictionary<IPlayer, int> _cachePlayerTypes = new Dictionary<IPlayer, int>(2);


        object _updateSyncLock = new object();
        Timer _updateTimeLimitTimer;
        int _updateInterval = 100;

        DateTime _updateTime;
        double _timeLimitValue;

        public GamePresenter(IPlayGameHandler playGameUC)
        {
            this.playGameUC = playGameUC;

            // TODO: This is temporary solution, it is better to use events
            //playGameUC.PropertyChanged += playGameUC_PropertyChanged;

            playGameUC.GameStarted += playGameUC_GameStarted;
            playGameUC.PlayerChanged += playGameUC_PlayerChanged;
            playGameUC.TurnCompleted += playGameUC_TurnCompleted;
            playGameUC.GameEnded += playGameUC_GameEnded;

            _updateTimeLimitTimer = new Timer((o) => { UpdateTimeLimitOnAllView(); });
        }

        #region Event handlers

        void playGameUC_GameStarted(object sender, GameStartedArgs e)
        {
            // store board and players list from event
            board = e.Board;
            playersList = e.PlayersList;

            // refresh player type dictionary cache
            // number them sequentially
            _cachePlayerTypes.Clear();

            int index = 0;
            foreach (var p in playersList)
            {
                _cachePlayerTypes[p] = index;

                index++;
            }

            // updates all view
            lock (Views)
            {
                Views.ToList().ForEach(x =>
                    {
                        x.DisableStart();
                        x.EnableStop();

                        // create grid with cells
                        x.ResetBoard(board.SizeM, board.SizeN);

                        // fill only necessary cells with squares
                        for (int i = 0; i < board.SizeM; i++)
                            for (int j = 0; j < board.SizeN; j++)
                                if (board[i, j] != null)
                                    x.AddSquare(i, j);
                    });
            }
        }

        void playGameUC_PlayerChanged(object sender, PlayerChangedArgs e)
        {
            // store reference to the current player
            currentPlayer = e.CurrentPlayer;

            // update player name
            lock (Views)
            {
                Views.ToList().ForEach(x => x.SetPlayerName(e.CurrentPlayer.Name));
            }

            // start timer that countdowns time limit
            lock (_updateSyncLock)
            {
                // save time when we start process
                _updateTime = DateTime.Now;

                // save initial time limit value
                _timeLimitValue = e.TurnTimeLimit;
            }

            // start timer
            _updateTimeLimitTimer.Change(0, _updateInterval);
        }

        void playGameUC_TurnCompleted(object sender, TurnCompletedArgs e)
        {
            // stop timer after each turn
            _updateTimeLimitTimer.Change(Timeout.Infinite, Timeout.Infinite);

            // notify player that they are deactivated
            if (e.IsPlayerActive == false)
            {
                lock (Views)
                {
                    Views.ToList().ForEach(x => 
                        x.ShowTurnResult("Time is out. You are deactivated. :)"));
                }
            }

            // notify players who has won the game
            if (e.TurnResult.IsGameEnded)
            {
                lock (Views)
                {
                    Views.ToList().ForEach(x => x.ShowTurnResult(e.TurnResult.Message));
                }
            }
        }

        void playGameUC_GameEnded(object sender, GameEndedArgs e)
        {
            lock (Views)
            {
                Views.ToList().ForEach(x =>
                    {
                        x.EnableStart();
                        x.DisableStop();
                    });
            }
        }
        #endregion

        void UpdateTimeLimitOnAllView()
        {
            lock (Views)
            {
                lock (_updateSyncLock)
                {
                    var diffTime = (DateTime.Now - _updateTime).TotalSeconds;

                    var leftTime = _timeLimitValue - diffTime;

                    if (leftTime > 0)
                        Views.ToList().ForEach(x => x.SetTimeLimit(leftTime.ToString()));
                    else
                        Views.ToList().ForEach(x => x.SetTimeLimit("Time is up."));
                }
            }
        }
        
        #region PresenterBase Implementation
        /// <summary>
        /// Refresh a view with the initial state.
        /// </summary>
        /// <param name="viewInstance">View instance being wired up</param>
        protected override void RefreshView(IGameView viewInstance)
        {
            if (viewInstance == null)
                throw new ArgumentNullException("viewInstance");

            viewInstance.EnableStart();
            viewInstance.DisableStop();
           
            //TODO: refresh control
        }

        /// <summary>
        /// Obtain a reference to ourselves.
        /// </summary>
        /// <returns>This instance (as IClockPresenter)</returns>
        protected override IGamePresenter GetPresenterEndpoint()
        {
            return this;
        } 
        #endregion

        public void StartGame()
        {
            playGameUC.PlayGame();
        }

        public void SquareClicked(int i, int j)
        {
            // because after placing we release game and it will change a player
            // store type before any actions
            // TODO: should be redesigned
            var pieceType = _cachePlayerTypes[currentPlayer];

            // if validation passed
            if (playGameUC.PlacePiece(board[i, j]))
            {
                lock (Views)
                {
                    Views.ToList().ForEach(x =>
                    {
                        x.PlacePiece(i, j, pieceType);
                    });
                }
            }
        }
    }
}
