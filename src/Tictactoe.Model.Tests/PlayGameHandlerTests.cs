﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using Tictactoe.Contracts.Model;
using System.Collections.Generic;

namespace Tictactoe.Model.Tests
{
    [TestClass]
    public class PlayGameHandlerTests
    {
        [TestMethod]
        [TestCategory("CI")]
        public void PlayGameHandler_CallingRunGame_CallsSessionRestart()
        {
            // assign
            var gameSessionMock = new Mock<IGameSession>();

            var handler = new PlayGameHandler(gameSessionMock.Object);

            // act
            handler.RunGame();

            // assert
            gameSessionMock.Verify(x => x.Restart(), Times.Once, "PlayerGameHandler must restart IGameSession.");
        }

        [TestMethod]
        [TestCategory("CI")]
        public void PlayGameHandler_CallingRunGame_CallsGetFirstPlayerAndRaisesEvent()
        {
            // assign
            var playerMock = new Mock<IPlayer>();

            var turnResultsMock = new Mock<ITurnResult>();

            var gameSessionMock = new Mock<IGameSession>();
            
            gameSessionMock.Setup(x => x.GetFirstPlayer()).Returns(playerMock.Object);

            gameSessionMock.Setup(x => x.GetTurnResult()).Returns(turnResultsMock.Object);

            var handler = new PlayGameHandler(gameSessionMock.Object);

            // subscribe to the event
            bool eventRaised = false;

            handler.PlayerChanged += (s, e) =>
                {
                    eventRaised = true;
                };

            // act
            handler.RunGame();

            // assert
            gameSessionMock.Verify(x => x.GetFirstPlayer(), Times.Once,
                "PlayerGameHandler should set first player to make a turn.");

            Assert.IsTrue(eventRaised, "PlayerGameHandler should raise event with changes");
        }

        [TestMethod]
        [TestCategory("CI")]
        public void PlayGameHandler_CallingRunGame_CallsGetTimeLimitAndRaisesEvent()
        {
            // assign
            var playerMock = new Mock<IPlayer>();

            var turnResultsMock = new Mock<ITurnResult>();

            var gameSessionMock = new Mock<IGameSession>();

            gameSessionMock.Setup(x => x.GetFirstPlayer())
                .Returns(playerMock.Object);

            gameSessionMock.Setup(x => x.GetTurnResult()).Returns(turnResultsMock.Object);

            var handler = new PlayGameHandler(gameSessionMock.Object);

            // subscribe to the event
            bool eventRaised = false;

            handler.PlayerChanged += (s, e) =>
                {
                    eventRaised = true;
                };

            // act
            handler.RunGame();

            // assert
            gameSessionMock.Verify(x => x.GetTimeLimit(It.IsAny<IPlayer>()), Times.Once,
                "PlayerGameHandler should get time limits.");

            Assert.IsTrue(eventRaised, "PlayerGameHandler should raise event with changes.");
        }

        [TestMethod]
        [TestCategory("CI")]
        public void PlayGameHandler_CallingRunGame_CallsMakeMove()
        {
            // assign
            var playerMock = new Mock<IPlayer>();

            var turnResultsMock = new Mock<ITurnResult>();

            var gameSessionMock = new Mock<IGameSession>();

            gameSessionMock.Setup(x => x.GetFirstPlayer()).Returns(playerMock.Object);

            gameSessionMock.Setup(x => x.GetTurnResult()).Returns(turnResultsMock.Object);


            var handler = new PlayGameHandler(gameSessionMock.Object);

            // act
            handler.RunGame();

            // assert
            gameSessionMock.Verify(x => x.MakeMove(It.IsAny<IPlayer>()), Times.Once,
                "PlayerGameHandler should make a move for player.");
        }

        [TestMethod]
        [TestCategory("CI")]
        public void PlayGameHandler_CallingRunGame_ReturnsTurnResultAndGetNextPlayer()
        {
            // assign
            var playerMock = new Mock<IPlayer>();

            var turnResultsMock = new Mock<ITurnResult>();

            var gameSessionMock = new Mock<IGameSession>();

            gameSessionMock.Setup(x => x.GetFirstPlayer()).Returns(playerMock.Object);

            gameSessionMock.Setup(x => x.GetTurnResult()).Returns(turnResultsMock.Object);


            var handler = new PlayGameHandler(gameSessionMock.Object);

            // subscribe to the event
            bool eventRaised = false;

            handler.TurnCompleted += (s, e) =>
                {
                    eventRaised = true;
                };

            // act
            handler.RunGame();

            // assert
            playerMock.VerifyGet(x => x.Active, Times.Once,
                "PlayerGameHandler should get player activity status.");

            gameSessionMock.Verify(x => x.GetTurnResult(), Times.Once,
                "PlayerGameHandler should ask to calculate turn results.");

            Assert.IsTrue(eventRaised, "PlayerGameHandler should raise event about turn results.");

            gameSessionMock.Verify(x => x.GetNextPlayer(It.IsAny<IPlayer>()), Times.Once,
                "PlayerGameHandler should asks to get next player.");
        }

        [TestMethod]
        [TestCategory("CI")]
        public void PlayGameHandler_CallingPlacePiece_CallsGameSessionPlacePiece()
        {
            // assign
            var squareMock = new Mock<ISquare>();

            var gameSessionMock = new Mock<IGameSession>();

            var handler = new PlayGameHandler(gameSessionMock.Object);

            // act
            handler.PlacePiece(squareMock.Object);
            
            // assert
            gameSessionMock.Verify(x => x.PlacePiece(squareMock.Object, It.IsAny<IPlayer>()),
                Times.Once, "PlayerGameHandler should ask to calculate turn results.");
        }
    }
}
