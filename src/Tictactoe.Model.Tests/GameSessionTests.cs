﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using Tictactoe.Model;
using Tictactoe.Contracts.Model;
using System.Collections.Generic;
using System.Linq;
using System.Threading;

namespace Tictactoe.Model.Tests
{
    [TestClass]
    public class GameSessionTests
    {
        [TestMethod]
        [TestCategory("Time dependent")]
        public void GameSession_WhenMakingMove_MakesPlayerMoveAndUpdatesTimeLimit()
        {
            // ASSING
            // time limit in seconds
            double initialTimeLimit =  5.0;

            // player will wait when make a move
            var playerMock = new Mock<IPlayer>();
            playerMock.Setup(x => x.MakeMove(It.IsAny<IGameSession>(), It.IsAny<double>()))
                .Callback(() => 
                    {
                        Thread.Sleep((int)Math.Round(initialTimeLimit * 1000));
                    });

            // return the same time limit for all players
            var gameRuleMock = new Mock<IGameRule>();

            gameRuleMock.Setup(x => x.GetTimeLimit(It.IsAny<IPlayer>()))
                .Returns(initialTimeLimit);

            // it should be OK if we use only one player
            var session = new GameSession(
                new List<IPlayer>() { playerMock.Object },
                gameRuleMock.Object);

            // ACT
            session.Restart();

            DateTime startTime = DateTime.Now;

            session.MakeMove(playerMock.Object);

            DateTime endTime = DateTime.Now;

            var actualTimeLimit = session.GetTimeLimit(playerMock.Object);

            // ASSERT
            // check that MakeMove have called
            playerMock.Verify(x => x.MakeMove(session, initialTimeLimit), Times.Once);

            // check that time limit update is correct
            var diffTime = (endTime - startTime).TotalSeconds;

            Assert.AreEqual(initialTimeLimit - diffTime, actualTimeLimit, 0.5,
                "Session should correctly update player time limit.");
        }

        [TestMethod]
        [TestCategory("CI")]
        public void GameSession_WhenPlacingPiece_RaisesEventOnlyWhenAllowed()
        {
            // ASSING
            var squareMock = new Mock<ISquare>();
            squareMock.Setup(x => x.IsEmpty).Returns(true);

            var playerMock = new Mock<IPlayer>();

            var gameRuleMock = new Mock<IGameRule>();

            // it should be OK if we use only one player
            var session = new GameSession(
                new List<IPlayer>() { playerMock.Object },
                gameRuleMock.Object);

            // subscribe to the event and 
            int raisesCounter = 0;
            session.PiecePlaced += (sender, e) => raisesCounter++;

            // ACT
            // without raise
            session.PlacePiece(squareMock.Object, playerMock.Object);

            session.AllowPiecePlacing();
            // with raise
            session.PlacePiece(squareMock.Object, playerMock.Object);

            session.DenyPiecePlacing();

            // without riase
            session.PlacePiece(squareMock.Object, playerMock.Object);

            // ASSERT
            Assert.AreEqual(1, raisesCounter, "Event raises wrong number of times.");
        }
        
        [TestMethod]
        [TestCategory("CI")]
        public void GameSession_WhenPlacingPiece_AddsPieceToSquare()
        {
            // ASSING
            var squareMock = new Mock<ISquare>();
            squareMock.Setup(x => x.IsEmpty).Returns(true);

            var playerMock = new Mock<IPlayer>();

            var gameRuleMock = new Mock<IGameRule>();

            // it should be OK if we use only one player
            var session = new GameSession(
                new List<IPlayer>() { playerMock.Object },
                gameRuleMock.Object);

            // ACT
            session.AllowPiecePlacing();

            bool res = session.PlacePiece(squareMock.Object, playerMock.Object);

            // ASSERT
            playerMock.Verify(x => x.MakePiece(), Times.Once,
                "Session should ask IPlayer to create a piece.");

            squareMock.Verify(x => x.PlacePiece(It.IsAny<IPiece>()), Times.Once,
                "Session should place piece on the square.");

            Assert.IsTrue(res, "Placing piece on the empty square returns false.");

        }

        [TestMethod]
        [TestCategory("CI")]
        public void GameSession_WhenRestarting_MakesPlayersActive()
        {
            // ASSIGN
            int playersCount = 7;

            // setup player mocks list
            var playerMocks = new List<Mock<IPlayer>>();

            for (int i = 0; i < playersCount; i++)
            {
                var mock = new Mock<IPlayer>();

                mock.SetupProperty(x => x.Active, false);

                playerMocks.Add(mock);
            }

            // setup game rule
            var gameRuleMock = new Mock<IGameRule>();

            // get IPlayer list from mocks
            var playersList = playerMocks.Select(m => m.Object).ToList();

            var session = new GameSession(playersList, gameRuleMock.Object);

            // ACT 
            session.Restart();

            // ASSERT
            // player should be activated only once
            playerMocks.ForEach(m => m.VerifySet(x => x.Active = true, Times.Once, "All players should be activated."));
        }

        [TestMethod]
        [TestCategory("CI")]
        public void GameSession_WhenRestarting_AcksTimeLimitsFromRules()
        {
            // ASSIGN
            int playersCount = 4;

            // setup player mocks list
            var playerMocks = new List<Mock<IPlayer>>();

            for (int i = 0; i < playersCount; i++)
            {
                var mock = new Mock<IPlayer>();

                playerMocks.Add(mock);
            }

            // setup game rule
            var gameRuleMock = new Mock<IGameRule>();

            // GetTimeLimit method will return player HashCode as time limit value
            gameRuleMock.Setup(x => x.GetTimeLimit(It.IsAny<IPlayer>()))
                .Returns((IPlayer p) => p.GetHashCode());

            // get IPlayer list from mocks
            var playersList = playerMocks.Select(m => m.Object).ToList();

            var session = new GameSession(playersList, gameRuleMock.Object);

            // ACT 
            session.Restart();

            // ASSERT
            // check that GetTimeLimit called only once
            gameRuleMock.Verify(x => x.GetTimeLimit(It.IsAny<IPlayer>()),
                Times.Exactly(playersCount), "Expects that GetTimeLimit will be called once for each player.");

            // check that values have been stored
            playersList.ForEach(m =>
                {
                    Assert.AreEqual(m.GetHashCode(), session.GetTimeLimit(m),
                        "Supposed that IGameSession only stores values retrieved from IGameRule.");
                });
        }


        [TestMethod]
        [TestCategory("CI")]
        public void GameSession_WhenRestarting_CreatesBoardAndStoreIt()
        {
            // ARRANGE
            var board = new Mock<IBoard>();

            var playerMock = new Mock<IPlayer>();

            var gameRuleMock = new Mock<IGameRule>();
            gameRuleMock.Setup(x => x.MakeBoard()).Returns(board.Object);

            // it should be OK if we use only one player
            var session = new GameSession(new List<IPlayer>() { playerMock.Object }, 
                gameRuleMock.Object);
            // ACT
            session.Restart();

            // ASSERT
            gameRuleMock.Verify(x => x.MakeBoard(), Times.Once, "GameSession should ask IGameRule to create IBoard.");

            Assert.AreSame(board.Object, session.Board, "GameSession should store IBoard reference and allow to get it.");
        }
    }
}
