﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Tictactoe.Contracts.Model;
using Moq;
using System.Collections.Generic;
using System.Linq;

namespace Tictactoe.Model.Tests
{
    [TestClass]
    public class GameRuleTests
    {
        [TestMethod]
        [TestCategory("CI")]
        public void GameRule_AfterCreation_CanMakeBoard()
        {
            // assign
            var gameRule = new GameRule();

            // act
            var board = gameRule.MakeBoard();

            // assert
            Assert.IsNotNull(board, "Created board shouldn't be null.");

            Assert.IsTrue(board.SizeM > 0, "Board's size M should be greater than zero.");

            Assert.IsTrue(board.SizeN > 0, "Board's size N should be greater than zero.");
        }

        [TestMethod]
        [TestCategory("CI")]
        public void GameRule_GettingFirstPlayer_PlayerExistsInCollection()
        {
            // assign
            int playersCount = 7;

            // setup player mocks list
            var playerMocks = new List<Mock<IPlayer>>();

            for (int i = 0; i < playersCount; i++)
            {
                var mock = new Mock<IPlayer>();

                mock.SetupProperty(x => x.Active, true);

                playerMocks.Add(mock);
            }

            // get IPlayer list from mocks
            var playersList = playerMocks.Select(m => m.Object).ToList();

            var gameRule = new GameRule();

            // act
            var actual = gameRule.GetFirstPlayer(playersList);

            // assert
            Assert.IsNotNull(actual, "PlayerList contains active players it should be some one to play first.");

            CollectionAssert.Contains(playersList, actual, "Player should be from PlayersList");
        }

        [TestMethod]
        [TestCategory("CI")]
        public void GameRule_GettingNextPlayer_PlayerExistsInCollection()
        {
            // assign
            int playersCount = 7;

            // setup player mocks list
            var playerMocks = new List<Mock<IPlayer>>();

            for (int i = 0; i < playersCount; i++)
            {
                var mock = new Mock<IPlayer>();

                mock.SetupProperty(x => x.Active, true);

                playerMocks.Add(mock);
            }

            // get IPlayer list from mocks
            var playersList = playerMocks.Select(m => m.Object).ToList();

            // get last one
            var currentPlayer = playersList[playersCount - 1];
            
            // get cyclic next, i.e., first one
            var simpleNextPlayer = playersList[0];

            var gameRule = new GameRule();

            // act
            var actual = gameRule.GetNextPlayer(playersList, currentPlayer);

            // assert
            Assert.IsNotNull(actual, "PlayerList contains active players it should be some one to play next.");

            CollectionAssert.Contains(playersList, actual, "Player should be from PlayersList");

            Assert.AreSame(simpleNextPlayer, actual, 
                "Next player selection doesn't match simple strategy.YOU CAN CHANGE THIS.");
        }
    }
}
