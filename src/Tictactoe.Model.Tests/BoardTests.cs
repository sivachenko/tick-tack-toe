﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Tictactoe.Model;

namespace Tictactoe.Model.Tests
{
    [TestClass]
    public class BoardTests
    {
        [TestMethod]
        public void Board_WhenCreating_CreatesSquares()
        {
            // assign
            int[,] boardTemplate = {                                     
                {1, 1, 0},
                {0, 1, 1}};

            // act
            var board = new Board(boardTemplate);

            // assert
            Assert.AreEqual(2, board.SizeM, "Size M is wrong.");

            Assert.AreEqual(3, board.SizeN, "Size N is wrong.");

            Assert.IsNotNull(board[0, 0], "Square is created with mistake.");
            Assert.IsNotNull(board[0, 1], "Square is created with mistake.");
            Assert.IsNull(board[0, 2], "Square is created with mistake.");

            Assert.IsNull(board[1, 0], "Square is created with mistake.");
            Assert.IsNotNull(board[1, 1], "Square is created with mistake.");
            Assert.IsNotNull(board[1, 2], "Square is created with mistake.");
        }
    }
}
