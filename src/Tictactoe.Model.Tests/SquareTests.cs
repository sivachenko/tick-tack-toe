﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Tictactoe.Model;
using Moq;
using Tictactoe.Contracts.Model;

namespace Tictactoe.Model.Tests
{
    [TestClass]
    public class SquareTests
    {
        [TestMethod]
        [TestCategory("CI")]
        public void Square_WhenCreated_IsEmpty()
        {
            // assign

            // act
            var square = new Square();

            bool actual = square.IsEmpty;

            // assert
            Assert.IsTrue(actual, "Square should be created empty.");
        }

        [TestMethod]
        [TestCategory("CI")]
        public void Square_PlacingPiece_StoresValue()
        {
            // assign
            var pieceMock = new Mock<IPiece>();

            var square = new Square();

            // act
            square.PlacePiece(pieceMock.Object);

            var actualValue = square.GetPiece();
            var actualState = square.IsEmpty;

            // assert
            Assert.AreSame(pieceMock.Object, actualValue,
                "Square should store reference to IPiece without modification.");

            Assert.IsFalse(actualState, "Square should become non-empty after placing a piece.");
        }
    }
}
