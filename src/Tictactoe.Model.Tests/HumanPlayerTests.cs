﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using Tictactoe.Model;
using Tictactoe.Contracts.Model;
using System.Collections.Generic;
using System.Linq;
using System.Threading;

namespace Tictactoe.Model.Tests
{
    [TestClass]
    public class HumanPlayerTests
    {
        [TestMethod]
        [TestCategory("Time dependent")]
        public void HumanPlayer_WhenMakingMove_WaitsRightTime()
        {
            // ASSIGN
            var gameSessionMock = new Mock<IGameSession>();

            var player = new HumanPlayer();
            player.Active = true;
            
            // we are going to wait a several fractional seconds
            double timeLimit = 5.5;

            // ACT
            var beginTime = DateTime.Now;

            player.MakeMove(gameSessionMock.Object, timeLimit);

            var endTime = DateTime.Now;

            // ASSERT
            var diffTime = (endTime - beginTime).TotalSeconds;

            Assert.AreEqual(timeLimit, diffTime, 0.2, "Player move duration was wrong.");
        }

        [TestMethod]
        [TestCategory("CI")]
        public void HumanPlayer_WhenMakingMove_ManagesSessionPiecePlacing()
        {
            // ASSIGN
            var gameSessionMock = new Mock<IGameSession>();

            var player = new HumanPlayer();
            player.Active = true;

            double timeLimit = 0;

            // ACT
            player.MakeMove(gameSessionMock.Object, timeLimit);

            // ASSERT
            gameSessionMock.Verify(x => x.AllowPiecePlacing(), Times.Once,
                "HumanPlayer should enable piece placing.");

            gameSessionMock.Verify(x => x.DenyPiecePlacing(), Times.Once,
                "HumanPlayer should disable piece placing.");
            
        }

        [TestMethod]
        [TestCategory("Time dependent")]
        public void HumanPlayer_WhenMakingMove_ReceivesPiecePlacedEvent()
        {
            // ASSIGN
            var gameSessionMock = new Mock<IGameSession>();

            var player = new HumanPlayer();
            player.Active = true;

            // in seconds
            double timeLimit = 5;

            // in seconds
            double delayOfRaisingEvent = 2.5;

            // ACT
            double diffTime = -1;

            TimerCallback callback = (obj) =>
                {
                    // player shouldn't need any valid EventArgs
                    gameSessionMock.Raise(m => m.PiecePlaced += null, 
                        new PiecePlacedEventArgs());
                };

            // setup timer to raise event after some delay
            using (var timer = new Timer(callback, null, 
                (int)Math.Round(delayOfRaisingEvent * 1000), Timeout.Infinite))
            {
                // make call and measure elapsed time
                var beginTime = DateTime.Now;

                player.MakeMove(gameSessionMock.Object, timeLimit);

                var endTime = DateTime.Now;

                // calculate duration
                diffTime = (endTime - beginTime).TotalSeconds;
            }

            // ASSERT
            Assert.AreEqual(delayOfRaisingEvent, diffTime, 0.2, 
                "Player MakeMove should return after placing a piece is completed.");
        }
    }
}
