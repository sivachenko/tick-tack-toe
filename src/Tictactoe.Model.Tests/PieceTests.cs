﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using Tictactoe.Contracts.Model;

namespace Tictactoe.Model.Tests
{
    [TestClass]
    public class PieceTests
    {
        [TestMethod]
        [TestCategory("CI")]
        public void Piece_AfterCreation_CanReturnPieceOwner()
        {
            // assign
            var playerMock = new Mock<IPlayer>();

            // act
            var piece = new Piece(playerMock.Object);

            var actual = piece.GetPieceOwner();

            // assert
            Assert.AreSame(playerMock.Object, actual, "Piece cann't store IPlayer class.");
        }
    }
}
