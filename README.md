# Tick-tack-toe game #

Demonstration project.

# Design Features #

1. Style of squares can be defined independently in resources of each window.
2. Appearance of pieces can be defined via styles independently.
3. Number of players and pieces are not limited.
4. Size of a board in squares are not limited. 
5. The board can be rectangular. Winning combination check algorithm allows winning on 45-degree diagonals.
6. Configuration of the board can be defined by array template in GameRule class.
7. The board can contain void squares. You cannot place a piece on this square. Void square corresponds to zero in array template.
8. Game rules can define total limit of the player turns. Player who exceeds limit cannot make a move any more.
9. Each player has time penalty, which will be subtracted from the limit.

# Drawbacks #

1. Design and implementation are not optimized. There are several places where values are recalculated each time. At least:
	- when getting next player;
	- when checking winning combination.
2. Many unit tests are coupled in one test method.
3. Stop button exits the application; it should carefully stop the thread (set flag and mutex).
4. Call of method IGameSession.PlacePiece should be redesigned. It is better to:
	- Invert of Control at this point to send validation callback to IPresenter (to prevent future mistakes).
	- Move IsEmpty check to IGameRule. 
4. The next step is to implement StartupHandle, which will use Ninject. Now using Ninject seems unnecessary.
5. I prefer MVP design pattern, but I think MVVM was expected.
6. Somehow, Modelling tool in Visual Studio does not generate code of event of interface. May be I miss something. 
Therefore, contracts were generated in separate project and copied to project with contracts.
7. I miss logging, exception handling and attractive interface.
8. The code should be commented much more.


P.S. I've got some fun completing the task.  :)